package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("<head>\r\n");
      out.write("\t<meta charset=\"UTF-8\">\r\n");
      out.write("\t<title>Geolocalizacion</title>\r\n");
      out.write("\t<style>\r\n");
      out.write("\t\t#map{\r\n");
      out.write("\t\t\tmargin: 20px;\r\n");
      out.write("\t\t}\r\n");
      out.write("\t</style>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<button onclick=\"findMe()\">Mostrar ubicación</button>\r\n");
      out.write("\t<div id=\"map\"></div>\r\n");
      out.write("\t\r\n");
      out.write("\t\t<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCS9EfM4T7VXOasRvTX4IxdLfQptyziaSc&signature=ieDoztaF4tcC3B2rVRtr6Z9nXnQ=\"</script>\r\n");
      out.write("\t<script>\r\n");
      out.write("\t\tfunction findMe(){\r\n");
      out.write("\t\t\tvar output = document.getElementById('map');\r\n");
      out.write("\t\t\t// Verificar si soporta geolocalizacion\r\n");
      out.write("\t\t\tif (navigator.geolocation) {\r\n");
      out.write("\t\t\t\toutput.innerHTML = \"<p>Tu navegador soporta Geolocalizacion</p>\";\r\n");
      out.write("\t\t\t}else{\r\n");
      out.write("\t\t\t\toutput.innerHTML = \"<p>Tu navegador no soporta Geolocalizacion</p>\";\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\t//Obtenemos latitud y longitud\r\n");
      out.write("\t\t\tfunction localizacion(posicion){\r\n");
      out.write("\t\t\t\tvar latitude = posicion.coords.latitude;\r\n");
      out.write("\t\t\t\tvar longitude = posicion.coords.longitude;\r\n");
      out.write("\t\t\t\tvar imgURL = \"https://maps.googleapis.com/maps/api/staticmap?center=\"+latitude+\",\"+longitude+\"&size=600x300&markers=color:red%7C\"+latitude+\",\"+longitude+\"&key=https://maps.googleapis.com/maps/api/js?key=AIzaSyCS9EfM4T7VXOasRvTX4IxdLfQptyziaSc&signature=ieDoztaF4tcC3B2rVRtr6Z9nXnQ=\";\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\tfunction error(){\r\n");
      out.write("\t\t\t\toutput.innerHTML = \"<p>No se pudo obtener tu ubicación</p>\";\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\tnavigator.geolocation.getCurrentPosition(localizacion,error);\r\n");
      out.write("\t\t}\r\n");
      out.write("\t</script>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
